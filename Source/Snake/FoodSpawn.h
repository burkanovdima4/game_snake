#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"


#include "FoodSpawn.generated.h"

class AFood;

UCLASS()
class SNAKE_API AFoodSpawn : public AActor
{
	GENERATED_BODY()
	
public:	
	AFoodSpawn();

	UPROPERTY(BlueprintReadWrite)
	AFood* FoodActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood> FoodClass;


protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void SpawningFood();

};
