#include "FieldGenerated.h"
#include "SnakeBase.h"
#include "FieldBlock.h"

AFieldGenerated::AFieldGenerated()
{
 
	PrimaryActorTick.bCanEverTick = true;

}

void AFieldGenerated::BeginPlay()
{
	Super::BeginPlay();
	SpawningField(6, 15);
}

void AFieldGenerated::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFieldGenerated::SpawningField(float x, float y)
{
	float Size = 60;
	FVector Location1, Location2, Location3, Location4;
	FRotator Rotation = { 0,0,0 };
	for (int i = (int) -x; i < (x+1); i++)
	{
		for (int j = (int) - y; j < (y+1); j++)
		{
			
			Location4 = { (Size * i), (Size * j),  (Size * -1) };
			FieldBlooks = GetWorld()->SpawnActor<AFieldBlock>(FieldBlockClass, Location4, Rotation);
		}
	}
	for (int i = (int) -x ; i < (x + 1); i++)
	{
		Location1 = { (Size * i), Size * (y + 1 ),  0};
		Location2 = { (- Size * i), Size * (-y - 1),  0 };
		FieldBlooks = GetWorld()->SpawnActor<AFieldBlock>(FieldBlockClass, Location1, Rotation);
		FieldBlooks = GetWorld()->SpawnActor<AFieldBlock>(FieldBlockClass, Location2, Rotation);
	}
	for (int i = (int) -y; i < (y + 1); i++)
	{
		Location1 = { Size * (x + 1), Size * i,  0 };
		Location2 = {  Size * (- x - 1), - Size * i,  0 };
		FieldBlooks = GetWorld()->SpawnActor<AFieldBlock>(FieldBlockClass, Location1, Rotation);
		FieldBlooks = GetWorld()->SpawnActor<AFieldBlock>(FieldBlockClass, Location2, Rotation);
	}
}


