#include "PlayerField.h"
#include "SnakeBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

APlayerField::APlayerField()
{
	PrimaryActorTick.bCanEverTick = true;
	SizeForX = 5;
	SizeForY = 5;
	BlocMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MesshComponent"));
	BlocMesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	BlocMesh->SetCollisionResponseToAllChannels(ECR_Overlap);
}

void APlayerField::BeginPlay()
{
	Super::BeginPlay();
}

void APlayerField::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APlayerField::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->Destroy();
		}
	}
}



