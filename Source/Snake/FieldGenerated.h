#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "FieldGenerated.generated.h"

class AFieldBlock;
class ASnakeBase;

UCLASS()
class SNAKE_API AFieldGenerated : public AActor
{
	GENERATED_BODY()
	
public:	
	AFieldGenerated();

	UPROPERTY(BlueprintReadWrite)
	AFieldBlock* FieldBlooks;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFieldBlock> FieldBlockClass;

	UPROPERTY(BlueprintReadWrite)
	ASnakeBase* Snake;



protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void SpawningField(float x, float y);
};
