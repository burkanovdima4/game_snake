#include "FieldBlock.h"
#include "SnakeBase.h"



AFieldBlock::AFieldBlock()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AFieldBlock::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AFieldBlock::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFieldBlock::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AllDestroy();
		}
	}
}

