#include "FoodSpawn.h"
#include "SnakeBase.h"
#include "Food.h"
#include "Math/UnrealMathUtility.h"



AFoodSpawn::AFoodSpawn()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AFoodSpawn::BeginPlay()
{
	Super::BeginPlay();
	SpawningFood();
}

void AFoodSpawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AFoodSpawn::SpawningFood()
{
	int Size = 60;
	float x = (float)(FMath::RandRange(-6, 6)* Size);
	float y = (float)(FMath::RandRange(-30, 30) * Size);
	FVector Location = { x ,y , 0 };
	FRotator Rotation = {0,0,0};
	GetWorld()->SpawnActor<AFood>(FoodClass, Location, Rotation);
}




