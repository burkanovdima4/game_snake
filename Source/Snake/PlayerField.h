// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"

#include "PlayerField.generated.h"

class UStaticMeshComponent;

UCLASS()
class SNAKE_API APlayerField : public AActor, public IInteractable
{
	GENERATED_BODY()

public:
	APlayerField();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* BlocMesh;

	UPROPERTY(EditDefaultsOnly)
	int SizeForX;

	UPROPERTY(EditDefaultsOnly)
	int SizeForY;




protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;


};
